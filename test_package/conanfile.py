import os
from conan import ConanFile
from conan.tools.build import can_run


class HP53132ATestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            if self.settings.os == "Windows":
                self.run("ds_HP53132A 2>&1 | findstr \"usage\"", env="conanrun")
            else:
                self.run("ds_HP53132A 2>&1 | grep \"usage\"", env="conanrun")
