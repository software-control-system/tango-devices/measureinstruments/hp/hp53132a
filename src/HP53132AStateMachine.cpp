static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/HP53132A/src/HP53132AStateMachine.cpp,v 1.4 2008-04-22 08:56:34 xavela Exp $";
//+=============================================================================
//
// file :         HP53132AStateMachine.cpp
//
// description :  C++ source for the HP53132A and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: xavela $
//
// $Revision: 1.4 $
//
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/09/25 12:23:07  sebleport
// - add SetAcquiring mode command.
// - single shot and continuous acquiring modes managed
// - modification in dev_state etc ...
//
// Revision 1.2  2006/11/14 17:19:51  sebleport
// -frequency attribute is now continously updated
// -the start and abort commands only concern the    couting measurement mode
//
// Revision 1.1.1.1  2006/08/02 07:39:54  stephle
// initial import
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <HP53132A.h>
#include <HP53132AClass.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace HP53132A_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_frequency_allowed
// 
// description : 	Read/Write allowed for frequency attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_frequency_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_countsNumber_allowed
// 
// description : 	Read/Write allowed for countsNumber attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_countsNumber_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_inputImpedance_allowed
// 
// description : 	Read/Write allowed for inputImpedance attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_inputImpedance_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_integrationTime_allowed
// 
// description : 	Read/Write allowed for integrationTime attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_integrationTime_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_countingCompleted_allowed
// 
// description : 	Read/Write allowed for countingCompleted attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_countingCompleted_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_frequencyMeasurementEnabled_allowed
// 
// description : 	Read/Write allowed for frequencyMeasurementEnabled attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_frequencyMeasurementEnabled_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_mode_allowed
// 
// description : 	Read/Write allowed for mode attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_mode_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_channelNumber_allowed
// 
// description : 	Read/Write allowed for channelNumber attribute.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_channelNumber_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_Reset_allowed
// 
// description : 	Execution allowed for Reset command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_Reset_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_Start_allowed
// 
// description : 	Execution allowed for Start command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_Start_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_Abort_allowed
// 
// description : 	Execution allowed for Abort command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_Abort_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_SetAcquisitionMode_allowed
// 
// description : 	Execution allowed for SetAcquisitionMode command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_SetAcquisitionMode_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_EnableCounterMode_allowed
// 
// description : 	Execution allowed for EnableCounterMode command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_EnableCounterMode_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		HP53132A::is_EnableFrequencyMode_allowed
// 
// description : 	Execution allowed for EnableFrequencyMode command.
//
//-----------------------------------------------------------------------------
bool HP53132A::is_EnableFrequencyMode_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

}	// namespace HP53132A
